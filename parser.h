#ifndef PARSER_H
#define PARSER_H

#include <stdint.h>
#include "opcode.h"

#define MAX_IDENT_LENGTH 512

enum directive { DIR_ALIGN };

struct directive_args {
    int number;
};

struct ast_callbacks {
    void (*instruction)(enum opcode, int arg1, int arg2, int arg3, int offset, void *data);
    void (*label_add)(char *symbol, int num, void *data);
    int (*label_address)(char *symbol, void *data);
    void (*store_byte)(uint8_t value, unsigned offset, void *data);
    void (*store_long)(uint32_t value, unsigned offset, void *data);
    void *user_data;
};

struct ast_node {
    union {
        struct {
            enum opcode op;
            int offset;
        } inst;
        int offset;
        int num;
        char *label;
    } val;
    struct ast_node *child[3];
    struct ast_node *next;
    struct ast_callbacks *cb;
    int (*eval)(struct ast_node*);
};

struct lexer {
    FILE *input;
    int state;
    int c;
    char buffer[MAX_IDENT_LENGTH];
    char *buffp;
    int line;
};

/* tokens starting with __ should be used only inside lexer code
 * i.e. get_token should never return these */
enum token {
    __START = 0,
    LEX_IDENT = 1,
    LEX_MINUS = 2,
    LEX_PLUS = 3,
    LEX_STAR = 4,
    LEX_SLASH = 5,
    LEX_NUM = 6,
    __ACCEPT = 7,
    LEX_ERROR = 8,
    LEX_LF = 9,
    LEX_EOF = 10,
    __COMMENT = 11,
    LEX_STRING = 12,
    __ESCSTR = 13,
    LEX_COLON = 14,
    LEX_COMMA = 15,
    LEX_LPAR = 16,
    LEX_RPAR = 17,
    LEX_AT = 18,

    __LEX_DIR_START,
    LEX_DIR_LONG,
    LEX_DIR_BYTE,
    LEX_DIR_SHORT,
    LEX_DIR_TEXT,
    LEX_DIR_DATA,
    LEX_DIR_TYPE,
    LEX_DIR_FILE,
    LEX_DIR_BALIGN,
    LEX_DIR_ALIGN,
    LEX_DIR_P2ALIGN,
    LEX_DIR_GLOBAL,
    LEX_DIR_GLOBL,
    LEX_DIR_SIZE,
    LEX_DIR_SECTION,
    LEX_DIR_STRING,
    LEX_DIR_IDENT,
    LEX_DIR_ASCIZ,
    __LEX_DIR_END,

    __OPCLASS_START,
    LEX_OPCLASS_NO_OPS,
    LEX_OPCLASS_REG,
    LEX_OPCLASS_REG_REG,
    LEX_OPCLASS_REG_REG_REG,
    LEX_OPCLASS_REG_IMM_REG,
    LEX_OPCLASS_REG_IMM,
    LEX_OPCLASS_IMM,
    __OPCLASS_END,

    LEX_NO_TOK
};

enum section { SECTION_TEXT, SECTION_DATA };

struct parser {
    struct lexer lex;
    enum token tok;
    int eof;
    struct ast_callbacks cb;
    unsigned mem_index;
    enum section cur_section;
};

struct ast_node *parser_line(struct parser *p);
void parser_init(struct parser *p, FILE *in, struct ast_callbacks cb);
    void (*rlabel_add)(char *symbol, int num, void *data);
    int (*ilabel_address)(char *symbol, void *data);

#endif
