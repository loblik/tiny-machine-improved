#ifndef OPCODE_H
#define OPCODE_H

/* XXX: these must be in pairs with strings in opcode.c */
enum opcode {

    op_HALT,    /* RR     halt, operands are ignored */
    op_RET,

    OP_CLASS_NO_OPS,

    op_IN,      /* RR     read into reg(r); s and t are ignored */
    op_OUT,     /* RR     write from reg(r), s and t are ignored */
    op_CALL,

    OP_CLASS_REG,

    op_MOV,
    op_INC,     /* RR    reg(r) = reg(r) + 1, s and t ignored */
    op_DEC,     /* RR    reg(r) = reg(r) - 1, s and t ignored */

    OP_CLASS_REG_REG,

    op_ADD,    /* RR     reg(r) = reg(s)+reg(t) */
    op_SUB,    /* RR     reg(r) = reg(s)-reg(t) */
    op_MUL,    /* RR     reg(r) = reg(s)*reg(t) */
    op_DIV,    /* RR     reg(r) = reg(s)/reg(t) */
    op_SHL,     /* RR    reg(r) = reg(r) << reg(t) */
    op_SHR,     /* RR    reg(r) = reg(r) >> reg(t) */
    op_SHA,
    op_XOR,
    op_AND,
    op_OR,

    OP_CLASS_REG_REG_REG,

    /* RM instructions */
    op_LD,      /* RM     reg(r) = mem(d+reg(s)) */
    op_LBS,
    op_LBZ,
    op_LHS,
    op_LHZ,
    op_ST,      /* RM     mem(d+reg(s)) = reg(r) */
    op_STB,
    op_STH,
    op_LDA,

    OP_CLASS_REG_IMM_REG,   /* Limit of RM opcodes */

    /* RA instructions */
    op_LDC,     /* RA     reg(r) = d; reg(s) is ignored */
    op_LDCsym,
    op_JLT,     /* RA     if reg(r)<0 then reg(7) = d+reg(s) */
    op_JLE,     /* RA     if reg(r)<=0 then reg(7) = d+reg(s) */
    op_JGT,     /* RA     if reg(r)>0 then reg(7) = d+reg(s) */
    op_JGE,     /* RA     if reg(r)>=0 then reg(7) = d+reg(s) */
    op_JEQ,     /* RA     if reg(r)==0 then reg(7) = d+reg(s) */
    op_JNE,     /* RA     if reg(r)!=0 then reg(7) = d+reg(s) */

    OP_CLASS_REG_IMM,

    op_JMP,

    OP_CLASS_IMM,
    OP_CLASS_INVALID
};

#define NUM_INSTRUCTIONS OP_CLASS_INVALID

enum opcode opcode_class(enum opcode);
enum opcode opcode_from_str(const char*);
const char *opcode_to_str(enum opcode op);
int opcode_length(enum opcode);

#endif
