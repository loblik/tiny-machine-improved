#ifndef TINY_STD
#define TINY_STD

#define CHAR_IN_ADDR    0x2
#define CHAR_OUT_ADDR   0x1

#define EOF 0

/* have to use volatile to let compiler know
 * and to prevent mindless optimizations */

int in() {
    return *((volatile char*)CHAR_IN_ADDR);
}

void out(int c) {
    (*(volatile char*)CHAR_OUT_ADDR) = c;
}

void prints(char *str) {
    while (*str)
        out(*str++);
}

int strlen(char *str) {
    int len = 0;
    while (*str++)
        len++;
    return len;
}

void *memcpy(void * s1, const void * s2, int n) {
    register char *r1 = s1;
    register const char *r2 = s2;

    while (n > 0) {
        *r1++ = *r2++;
        --n;
    }

    return s1;
}

#endif
