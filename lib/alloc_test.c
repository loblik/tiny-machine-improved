#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* this must be included after stdlib.h
 * to redefine malloc and free
 * stdlib.h is needed for rand() and
 * others */
#define DEBUG 1
#include "alloc.h"

#define SIZE 500

int rand_from(int max) {
    return rand() / (RAND_MAX / max + 1);
}

int main() {

    void *array[SIZE];

    int i;
    for (i = 0; i < SIZE; i++)
        array[i] = NULL;

    malloc_init();
    __heap_print();

    for (i = 0; i < SIZE; i++) {
        array[i] = malloc(4);
        __heap_print();
        if (!array[i]) {
            break;
        }
    }

    __heap_print();
    int allocated = i;
    printf("allocated: %d\n", allocated);

    int freed = 0;
    srand(time(NULL));

    while (freed < allocated) {
        int random = rand_from(allocated);
        printf("random: %d allocated: %d freed: %d\n", random, allocated, freed);
        if (array[random]) {
            free(array[random]);
            array[random] = NULL;
            __heap_print();
            freed++;
        }
    }

    __heap_print();
    return 0;
}
