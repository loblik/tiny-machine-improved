/* File: tm.c                                       */
/* The TM ("Tiny Machine") computer                 */
/* Compiler Construction: Principles and Practice   */
/* Kenneth C. Louden                                */
/****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <ctype.h>
#include <assert.h>

#include "cpool.h"
#include "debug.h"
#include "parser.h"

#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

#define MEM_SIZE  (1024*64)
#define NO_REGS 9

#define SP_REG  6
#define FP_REG  7
#define PC_REG  8

#define LINESIZE  121
#define WORDSIZE  20
#define HALT_OFFSET 15

/* stack
|  - OxFFFF
|  - stack grows down
V

+-------------------+
| caller setup args |
|                   |
+-------------------+
|  return address   |
+-------------------+ <- SP after call
| callee local vars |
|                   |
|                   |
+-------------------+
  - 0x000
*/


const char * stepResultTab[] = {
    "OK",
    "Halted",
    "Instruction Memory Fault",
    "Data Memory Fault",
    "Division by 0"
};

enum {
    OKAY,
    HALT,
    IMEM_ERR,
    DMEM_ERR,
    ZERODIVIDE,
};

/* in case instruction is using symbolic argument
 * like LDA 1,#global_var iarg operand is position
 * in constant pool */
struct inst {
    uint8_t iop;
    uint32_t iarg1;
    uint32_t iarg2;
    uint32_t iarg3;
    int fn_name_cpoll_offset;   /* to get function name for debugging */
};

struct inst_stat {
    unsigned int count[NUM_INSTRUCTIONS];
    unsigned int executed[NUM_INSTRUCTIONS];
};

struct tm {
    struct cons_pool cpool;
    struct inst_stat stats;

    uint8_t data_mem [MEM_SIZE];
    int32_t reg [NO_REGS];
};

void tm_store_val_8(struct tm *t, uint8_t value, int offset);

int tm_step(struct tm *t);
void tm_store_val_32(struct tm *t, uint32_t value, int offset);

char *tm_reg_alias[] = { "r0", "r1", "r2", "r3",
                         "r4", "r5", "SP", "FP", "PC" };

void inst_to_mem(struct inst *i, uint8_t *mem) {
    int len = opcode_length(i->iop);
    *(uint8_t*)mem = i->iop;
    mem += sizeof(i->iop);
    if (len == 1)
        return;
    *(uint32_t*)mem = i->iarg1;
    mem += sizeof(i->iarg1);
    if (len == 5)
        return;
    *(uint32_t*)mem = i->iarg2;
    mem += sizeof(i->iarg2);
    if (len == 9)
        return;
    *(uint32_t*)mem = i->iarg3;
    mem += sizeof(i->iarg3);
}

void inst_from_mem(struct inst *i, uint8_t *mem) {
    i->iop = *(uint8_t*)mem;
    mem += sizeof(i->iop);
    i->iarg1 = *(uint32_t*)mem;
    mem += sizeof(i->iarg1);
    i->iarg2 = *(uint32_t*)mem;
    mem += sizeof(i->iarg2);
    i->iarg3 = *(uint32_t*)mem;
    mem += sizeof(i->iarg3);
}

void inst_print(struct tm *t, struct inst *i) {
    printf("%6s", opcode_to_str(i->iop));
    switch (i->iop) {
        case op_SUB:
        case op_ADD:
        case op_SHL:
        case op_SHA:
        case op_SHR:
        case op_XOR:
        case op_AND:
        case op_OR:
            printf("%2d,%d,%d", i->iarg1, i->iarg2, i->iarg3);
            break;
        case op_LD:
        case op_ST:
        case op_LBZ:
        case op_LHZ:
        case op_LHS:
        case op_STH:
        case op_STB:
        case op_LDA:
        case op_LBS:
            printf("%2d,%d(%d)", i->iarg1, i->iarg2, i->iarg3);
            break;
        case op_MOV:
        case op_LDC:
            printf("%2d,%d", i->iarg1, i->iarg2);
            break;
        case op_LDCsym:
            assert(0 && "ldcsym");
            printf("%2d,%s", i->iarg1, cons_pool_offset_label(&t->cpool, i->iarg2));
            break;
        case op_CALL:
            printf("%2d (%s)", i->iarg1, cons_pool_offset_label(&t->cpool, t->reg[i->iarg1]));
            break;
        case op_JNE:
        case op_JEQ:
        case op_JLT:
        case op_JLE:
        case op_JGT:
        case op_JGE:
            printf("%2d,%d (%s)", i->iarg1, i->iarg2,
                cons_pool_offset_label(&t->cpool, i->iarg2));
            break;
        case op_JMP:
            printf(" %s", cons_pool_offset_label(&t->cpool, i->iarg1));
            break;
        case op_RET:
        case op_HALT:
            break;
        default:
            fprintf(stderr, "unknown opcode: %d (you maybe reading\
 data section or uninitialized memory)\n", i->iop);
    }
    printf ("\n");
}

int inst_print_raw(struct tm *t, unsigned offset) {
    struct inst i;
    inst_from_mem(&i, &t->data_mem[offset]);
    inst_print(t, &i);
    return opcode_length(i.iop);
}

void tm_regs_print(struct tm *t) {
    int i;
    printf("REGISTERS:\n");
    for (i = 0; i < NO_REGS; i++) {
        printf("  %s: 0x%08x (%4d)\n", tm_reg_alias[i], t->reg[i], t->reg[i]);
    }
}

void tm_reset(struct tm *t) {
    int i;
    for (i = 0; i < NO_REGS; i++)
        t->reg[i] = 0xf000 | i;

    t->reg[SP_REG] = MEM_SIZE - 4;
    tm_store_val_8(t, op_HALT, HALT_OFFSET);
    tm_store_val_32(t, HALT_OFFSET, t->reg[SP_REG]);

    int entry_point;
    if ((entry_point = cons_pool_find(&t->cpool, "main")) < 0) {
        fprintf(stderr, "cannot find label main (dunno where to start execution)\n");
        exit(EXIT_FAILURE);
    }
    t->reg[PC_REG] = entry_point;
    t->reg[FP_REG] = 0xdeadbeef;
}

int tm_run(struct tm *t, unsigned steps, int print) {
    int forever = !steps;
    int step_result;
    while (forever || steps) {
        if (print) {
            //int pc = t->reg[PC_REG];
            //int fn_cpool_index = t->code_mem[pc].fn_name_cpoll_offset;
            //printf("%s+%02d:\t", cons_pool_offset_label(&t->cpool, fn_cpool_index),
            //pc - cons_pool_pos_offset(&t->cpool, fn_cpool_index));
            printf("%5d", t->reg[PC_REG]);
            inst_print_raw(t, t->reg[PC_REG]);
        }
        step_result = tm_step(t);
        if (step_result != OKAY)
            break;
        steps--;
    }
    return step_result;
    // rep->steps--;
}

void tm_init(struct tm *t) {

    cons_pool_init(&t->cpool);
    int i;
    for (i = 0; i < MEM_SIZE; i++)
        t->data_mem[i] = 0;

    t->data_mem[HALT_OFFSET] = op_HALT;

    /* poit SP to the last 4 bytes of mem
     * stack is going downwards */
    //t->data_mem[0] = MEM_SIZE;
}

void tm_store_val_8(struct tm *t, uint8_t value, int offset) {
    t->data_mem[offset] = value & 0xff;
    return;
}

void tm_store_val_16(struct tm *t, uint32_t value, int offset) {
    t->data_mem[offset] = value & 0xff;
    t->data_mem[offset + 1] = (value >> 8) & 0xff;
    return;
}

void tm_store_val_32(struct tm *t, uint32_t value, int offset) {
    t->data_mem[offset] = value & 0xff;
    t->data_mem[offset + 1] = value >>  8 & 0xff;
    t->data_mem[offset + 2] = value >> 16 & 0xff;
    t->data_mem[offset + 3] = value >> 24 & 0xff;
    return;
}

int tm_parse_mem_locs(struct tm *t, char *line, int *offset, int *len) {
    int is_reg = 0;
    int chars_consumed;
    *len = 4;

    if (*line == '@') {
        is_reg = 1;
        line++;
    }

    if ((sscanf(line, "%d,%d%n", offset, len, &chars_consumed) != 2) &&
        (sscanf(line, "%d%n", offset, &chars_consumed) != 1)) {
        printf("invalid memory format\n");
        return 0;
    }

    if (is_reg) {
        chars_consumed++;
        if (*offset > NO_REGS || *offset < 0) {
            printf("invalid register number\n");
            return 0;
        }
        *offset = t->reg[*offset];
    }
    return chars_consumed;
}

int tm_dump_data(struct tm *t, int offset, int num) {
    uint32_t *data = (uint32_t*)&t->data_mem[offset];
    int i = 0;
    while (i < num && data < (uint32_t*)&t->data_mem[MEM_SIZE]) {
        printf("%5d 0x", offset + i*4);
        printf("%02x", *(((uint8_t*)data)+0));
        printf("%02x", *(((uint8_t*)data)+1));
        printf("%02x", *(((uint8_t*)data)+2));
        printf("%02x\n", *(((uint8_t*)data)+3));
        data++;
        i++;
    }
    return 1;
}

int tm_dump_instructions(struct tm *t, int offset, int num) {
    //int cpool_index = t->code_mem[start].fn_name_cpoll_offset;
    //DEBUG(printf("fn: %s +%d\n", cons_pool_offset_label(&t->cpool, cpool_index),
    //    start - cons_pool_pos_offset(&t->cpool, cpool_index));)
    int i = 0;
    for (; i < num; i++) {
        if (offset < 0 || offset >= MEM_SIZE) {
            printf("instruction offset out of memory\n");
            return -1;
        }
        printf("%5d", offset);
        offset += inst_print_raw(t, offset);
    }
    return 1;
}

void tm_load_reg(struct tm *t, int reg, int offset) {
    t->reg[reg] = (uint32_t)(t->data_mem[offset] |
        (t->data_mem[offset + 1] << 8) | (t->data_mem[offset + 2] << 16) |
            (t->data_mem[offset + 3] << 24));
}

int tm_step(struct tm *t) {
    struct inst currentinstruction;
    int pc;
    int ar, as, at, m;

    pc = t->reg[PC_REG];

    if ((pc < 0) || (pc > MEM_SIZE)) {
        printf("memerror\n");
        return IMEM_ERR;
    }

    inst_from_mem(&currentinstruction, &t->data_mem[pc]);

    switch (opcode_class(currentinstruction.iop)) {
        case OP_CLASS_REG_REG_REG:
        case OP_CLASS_REG_REG:
            ar = currentinstruction.iarg1;
            as = currentinstruction.iarg2;
            at = currentinstruction.iarg3;
            break;
        case OP_CLASS_REG_IMM_REG:
          ar = currentinstruction.iarg1;
          as = currentinstruction.iarg3;
          m = currentinstruction.iarg2 + t->reg[as];
          if (((m < 0) || (m > MEM_SIZE)) && currentinstruction.iop != op_LDA)
             return DMEM_ERR;
          break;
        case OP_CLASS_REG_IMM:
          ar = currentinstruction.iarg1;
          break;
        default:
          break;
    }

    switch (currentinstruction.iop) {
        case op_JEQ:
        case op_JNE:
        case op_JGT:
        case op_JGE:
        case op_JLT:
        case op_JLE:
            m = currentinstruction.iarg2;
            break;
    }

    switch (currentinstruction.iop) {
        /* RR instructions */
        case op_HALT :
            ar = currentinstruction.iarg1;
            as = currentinstruction.iarg2;
            at = currentinstruction.iarg3;
            return HALT;
        case op_IN:
            t->reg[ar] = getchar();
            /*
            do {
                printf("Enter value for IN instruction: ");
                fflush (stdin);
                fflush (stdout);
                //gets(in_Line);
                //lineLen = strlen(in_Line);
                //inCol = 0;
                //ok = getNum();
                //if (!ok) printf ("Illegal value\n");
                //else t->reg[ar] = num;
            } while (!ok);
            */
            break;
        case op_OUT:
            /*printf ("OUT instruction prints: %d\n", reg[r] );*/
            printf ("OUT: %c\n", t->reg[ar] );
            break;
        case op_ADD:  t->reg[ar] = t->reg[as] + t->reg[at];  break;
        case op_SUB:  t->reg[ar] = t->reg[as] - t->reg[at];  break;
        case op_MUL:  t->reg[ar] = t->reg[as] * t->reg[at];  break;
        case op_DIV:
            if ( t->reg[at] != 0 ) t->reg[ar] = t->reg[as] / t->reg[at];
            else return ZERODIVIDE;
            break;

        case op_INC:  t->reg[ar] = t->reg[ar] + 1; break;
        case op_DEC:  t->reg[ar] = t->reg[ar] - 1; break;
        case op_SHL:  t->reg[ar] = t->reg[as] << t->reg[at]; break;
        /* C standard doesn't say if >> is logical or arithmetic shift
         * it's implementation defined, we assume it's arithmetic on signed
         * (which usually is) */
        case op_SHR:  t->reg[ar] = (unsigned)t->reg[as] >> t->reg[at]; break;
        case op_SHA:  t->reg[ar] = t->reg[as] >> t->reg[at]; break;
        case op_XOR:  t->reg[ar] = t->reg[as] ^ t->reg[at]; break;
        case op_AND:  t->reg[ar] = t->reg[as] & t->reg[at]; break;
        case op_OR:  t->reg[ar] = t->reg[as] | t->reg[at]; break;
        /*************** RM instructions ********************/
        /* we need to load/store by bytes to make TM's endiandess
         * independet of the host platform */
        case op_LD: {
            tm_load_reg(t, ar, m);
            break;
        }
        case op_ST:
            tm_store_val_32(t, t->reg[ar], m);
            break;
        case op_STB:
            if (m == 0x1) {
                putc(t->reg[ar], stdout);
            } else
            tm_store_val_8(t, t->reg[ar], m);
            break;
        case op_STH:
            tm_store_val_16(t, t->reg[ar], m);
            break;
        case op_LBS:
        case op_LBZ:
            if (currentinstruction.iop == op_LBZ)
                t->reg[ar] = (uint32_t)t->data_mem[m];
            else
                t->reg[ar] = (int32_t)t->data_mem[m];
            if (m == 0x2) {
                if (getenv("TMI_CLI"))
                    printf("input char:");
                t->reg[ar] = getchar();
                if (t->reg[ar] == EOF)
                    t->reg[ar] = 0;
            }
            break;
        case op_LHS:
            t->reg[ar] = (int32_t)(t->data_mem[m] | (t->data_mem[m + 1] << 8));
            break;
        case op_LHZ:
            t->reg[ar] = (uint32_t)(t->data_mem[m] | (t->data_mem[m + 1] << 8));
            break;
        /*************** RA instructions ********************/
        case op_LDA:
            t->reg[ar] = m;
            break;
        case op_LDCsym:
            t->reg[ar] = currentinstruction.iarg2;
            break;
        case op_LDC:
            t->reg[ar] = currentinstruction.iarg2;
            break;
        case op_JLT:
            t->reg[PC_REG] = (t->reg[ar] <  0) ? m : t->reg[PC_REG] + opcode_length(op_JLT);
            break;
        case op_JLE:
            t->reg[PC_REG] = (t->reg[ar] <= 0) ? m : t->reg[PC_REG] + opcode_length(op_JLE);
            break;
        case op_JGT:
            t->reg[PC_REG] = (t->reg[ar] >  0) ? m : t->reg[PC_REG] + opcode_length(op_JGT);
            break;
        case op_JGE:
            t->reg[PC_REG] = (t->reg[ar] >= 0) ? m : t->reg[PC_REG] + opcode_length(op_JGE);
            break;
        case op_JEQ:
            t->reg[PC_REG] = (t->reg[ar] == 0) ? m : t->reg[PC_REG] + opcode_length(op_JEQ);
            break;
        case op_JNE:
            t->reg[PC_REG] = (t->reg[ar] != 0) ? m : t->reg[PC_REG] + opcode_length(op_JNE);
            break;
        case op_JMP:
            t->reg[PC_REG] = currentinstruction.iarg1;
            break;
        case op_CALL:
            /* printf("call pc is: %d\n", t->reg[PC_REG]); */
            t->reg[SP_REG] -= 4;
            tm_store_val_32(t, t->reg[PC_REG] + opcode_length(op_CALL), t->reg[SP_REG]);
            t->reg[PC_REG] = t->reg[currentinstruction.iarg1];
            break;
        case op_MOV:
            t->reg[ar] = t->reg[as];
            break;
        case op_RET:
            tm_load_reg(t, PC_REG, t->reg[SP_REG]);
            t->reg[SP_REG] += 4;
            break;
        default:
            fprintf(stderr, "unknown instruction opcode: %s\n",
                opcode_to_str(currentinstruction.iop));
            exit(EXIT_FAILURE);

       /* end of legal instructions */
    }

    switch (currentinstruction.iop) {
        case op_JEQ:
        case op_JNE:
        case op_JGT:
        case op_JGE:
        case op_JLE:
        case op_JLT:
        case op_JMP:
        case op_RET:
        case op_CALL:
            break;
        default:
            t->reg[PC_REG] = pc + opcode_length(currentinstruction.iop);
    }

    return OKAY;
}

void print_help() {
    printf("Commands are:\n");
    printf("   s(tep <n>      "\
           "Execute n (default 1) TM instructions\n");
    printf("   g(o            "\
           "Execute TM instructions until HALT\n");
    printf("   r(egs          "\
           "Print the contents of the registers\n");
    printf("   i(Mem <b <n>>  "\
           "Print n iMem locations starting at b\n");
    printf("   d(Mem <b <n>>  "\
           "Print n dMem locations starting at b\n");
    printf("   t(race         "\
           "Toggle instruction trace\n");
    printf("   p(rint         "\
           "Toggle print of total instructions executed"\
           " ('go' only)\n");
    printf("   c(lear         "\
           "Reset simulator for new execution of program\n");
    printf("   h(elp          "\
           "Cause this list of commands to be printed\n");
    printf("   q(uit          "\
           "Terminate the simulation\n");
    printf("   a(nalyze       "\
           "Analyze the compilation and the simulation\n");
    printf("   o(ffsets       "\
       "Print constant pool names and offsets\n");

}

struct repl {
    int traceflag;
    int icountflag;
    int steps;
    int til_halt;
    char buffer_cmd[LINESIZE];
    char buffer_prev[LINESIZE];
};

void repl_init(struct repl *r) {
    r->traceflag = 0;
    r->icountflag = 0;
    r->steps = 0;
    r->til_halt = 0;
    r->buffer_prev[0] = '\0';
}

int do_command(struct tm *t, struct repl *rep, FILE *f) {

    //printf("Enter command: ");
    fgets(rep->buffer_cmd, LINESIZE, f);

    rep->buffer_cmd[strlen(rep->buffer_cmd) - 1] = '\0';
    rep->buffer_cmd[strlen(rep->buffer_cmd)] = '\0';

    if (strlen(rep->buffer_cmd)) {
        strncpy(rep->buffer_prev, rep->buffer_cmd, LINESIZE);
    } else {
        strncpy(rep->buffer_cmd, rep->buffer_prev, LINESIZE);
    }

    //printf("|%s|\n", buffer_cmd);
    int step = OKAY;

    int i;
    for (i = 0; rep->buffer_cmd[i] != '\0'; i++) {

        switch (rep->buffer_cmd[i]) {
            case 't':
                rep->traceflag = !rep->traceflag;
                printf("Tracing now ");
                if (rep->traceflag)
                    printf("on.\n");
                else
                    printf("off.\n");
                break;
            case 'h':
                print_help();
                break;
            case 'a':
                //stepcnt = 0;
                break;
            case 'p':
                rep->icountflag = !rep->icountflag;
                printf("Printing instruction count now ");
                if (rep->icountflag)
                    printf("on.\n");
                else
                    printf("off.\n");
                break;
            case 's':
                printf("EXECUTED:\n");
                /* FIXME: num of steps as arg? */
                step = tm_run(t, 1, 1);
                break;
    //if ( atEOL ())  stepcnt = 1;
    //else if ( getNum ())  stepcnt = abs(num);
    //else   printf("Step count?\n");
    //break;
            case 'g':
                rep->til_halt = 1;
                break;
            case 'r' :
                tm_regs_print(t);
                break;
            case 'i' :
            case 'd' : {
                char cmd = rep->buffer_cmd[i];
                char next = rep->buffer_cmd[i + 1];
                int offset, len = 4;
                if (next == '@' || isdigit(next)) {
                    int ret = tm_parse_mem_locs(t, &rep->buffer_cmd[i + 1],
                                &offset, &len);
                    if (ret == 0) {
                        printf("invalid location format, type 'h' for help");
                        rep->buffer_cmd[i] = '\0';
                        break;
                    } else {
                        i += ret;
                    }
                } else {
                    if (cmd == 'i') {
                        len = 4;
                        offset = t->reg[PC_REG];
                    } else {
                        offset = t->reg[SP_REG];
                        len = MEM_SIZE - offset;
                    }
                }
                if (cmd == 'i') {
                    printf("CODE MEMORY AT %d\n", offset);
                    tm_dump_instructions(t, offset, len);
                } else {
                    printf("DATA MEMORY AT %d\n", offset);
                    tm_dump_data(t, offset, len);
                }
                }
                break;
            case 'c':
      //iloc = 0;
      //dloc = 0;
      //stepcnt = 0;
      //for (regNo = 0;  regNo < NO_REGS; regNo++)
      //      reg[regNo] = 0;
      //r->data_mem[0] = DADDR_SIZE - 1;
      //for (loc = 1; loc < DADDR_SIZE; loc++)
      //      dMem[loc] = 0;
      //break;
            case 'q':
                return FALSE;
            case 'o':
                cons_pool_print(&t->cpool);
                //cons_pool_print(&t->dcpool);
                break;
            default:
                printf("Command %c unknown.\n", rep->buffer_cmd[i]);
                break;
        }
    }
    return step;
}

void cb_instruction(enum opcode op, int arg1, int arg2, int arg3, int offset, void *data) {
    struct tm *tiny = (struct tm*)data;
    struct inst i;
    i.iop = op;
    i.iarg1 = arg1;
    i.iarg2 = arg2;
    i.iarg3 = arg3;
    inst_to_mem(&i, &tiny->data_mem[offset]);
}

void cb_label_add(char *symbol, int address, void *data) {
    struct tm *tiny = (struct tm*)data;
    cons_pool_add(&tiny->cpool, symbol, address);
}

int cb_label_address(char *symbol, void *data) {
    struct tm *tiny = (struct tm*)data;
    return cons_pool_find(&tiny->cpool, symbol);
}

void cb_store_long(uint32_t val, unsigned offset, void *data) {
    struct tm *tiny = (struct tm*)data;
    tm_store_val_32(tiny, val, offset);
}

void cb_store_byte(uint8_t value, unsigned offset, void *data) {
    struct tm *tiny = (struct tm*)data;
    tm_store_val_8(tiny, value, offset);
    return;
}

/********************************************/
/* E X E C U T I O N   B E G I N S   H E R E */
/********************************************/
int main(int argc, char * argv[]) {

    char prog_name[LINESIZE];

    if (argc != 2) {
        printf("usage: %s <filename>\n",argv[0]);
        printf("\n");
        printf("environmental variables\n");
        printf("    TMI_CLI    run in interactive mode\n");
        printf("    TMI_TRACE  print executed instructions\n");
        printf("    TMI_DEBUG  debug parsing and constant pool)\n");
        printf("\n");
        exit(1);
    }

    strncpy(prog_name, argv[1], LINESIZE);
    prog_name[LINESIZE - 1] = '\0';

    if (strchr(prog_name, '.') == NULL)
        strcat(prog_name, ".tm");

    FILE *fh = fopen(prog_name, "r");
    if (fh == NULL) {
        printf("file '%s' not found\n", prog_name);
        exit(EXIT_FAILURE);
    }

    struct repl rep;
    struct tm t;

    repl_init(&rep);
    tm_init(&t);

    struct parser pars;
    struct ast_callbacks cb;

    cb.user_data = &t;
    cb.instruction = &cb_instruction;
    cb.label_add = &cb_label_add;
    cb.label_address = &cb_label_address;
    cb.store_byte = &cb_store_byte;
    cb.store_long = &cb_store_long;

    parser_init(&pars, fh, cb);

    struct ast_node *root;
    root = parser_line(&pars);
    if (root == NULL) {
        printf("parser returned null\n");
        exit(EXIT_FAILURE);
    }

    tm_reset(&t);

    if (root)
        root->eval(root);

    int status;
    if (getenv("TMI_CLI") != NULL) {
        printf("TM  simulation (enter h for help)...\n");
        while ((status = do_command(&t, &rep, stdin)) == OKAY);
        printf("Simulation done.\n");
    } else {
        status = tm_run(&t, 0, getenv("TMI_TRACE") != NULL);
    }

    switch (status) {
        case HALT:
            exit(t.reg[0]);
        case DMEM_ERR:
            fprintf(stderr, "access to illegal address");
            break;
        case IMEM_ERR:
            fprintf(stderr, "jump to illegal address %d\n", t.reg[PC_REG]);
            break;
        default:
            assert(0 && "undefined status returned\n");
    }
    return 0;
}
