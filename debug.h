#ifndef DEBUG_H
#define DEBUG_H

#define DEBUG(s) if (getenv("TMI_DEBUG")) { s ; }

#endif
