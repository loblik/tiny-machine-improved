CC=gcc
CFLAGS=-g -O0 -Wall -pedantic

all: tmi

parser: parser.c opcode.c opcode.h parser.h
	$(CC) $(CFLAGS) -c opcode.c
	$(CC) $(CFLAGS) -c parser.c

tmi: tmi.o cpool.o
	$(CC) $(CFLAGS) -o tmi *.o

tmi.o: tmi.c debug.h parser
	$(CC) $(CFLAGS) -c tmi.c

cpool.o: cpool.h cpool.c
	$(CC) $(CFLAGS) -c cpool.c

clean:
	rm -rf *.o tmi
