#include <string.h>
#include <assert.h>
#include "opcode.h"

#include <stdio.h>

/* XXX: this must match the enum above */
const char *opcode_str[] = {
    "HALT",
    "RET",

    NULL,

    "IN",
    "OUT",
    "CALL",

    NULL,

    "MOV",
    "INC",
    "DEC",

    NULL,

    "ADD",
    "SUB",
    "MUL",
    "DIV",
    "SHL",
    "SHR",
    "SHA",
    "XOR",
    "AND",
    "OR",

    NULL,

    "LD",
    "LBS",
    "LBZ",
    "LHS",
    "LHZ",
    "ST",
    "STB",
    "STH",
    "LDA",

    NULL,

    "LDC",
    "LDCsym",
    "JLT",
    "JLE",
    "JGT",
    "JGE",
    "JEQ",
    "JNE",

    "NULL",

    "JMP",

    NULL,
    NULL
};

enum opcode opcode_class(enum opcode op) {
    if (op > 0 && op < OP_CLASS_NO_OPS)
        return OP_CLASS_NO_OPS;
    else if (op > OP_CLASS_NO_OPS && op < OP_CLASS_REG)
        return OP_CLASS_REG;
    else if (op > OP_CLASS_REG && op < OP_CLASS_REG_REG)
        return OP_CLASS_REG_REG;
    else if (op > OP_CLASS_REG_REG && op < OP_CLASS_REG_REG_REG)
        return OP_CLASS_REG_REG_REG;
    else if (op > OP_CLASS_REG_REG_REG && op < OP_CLASS_REG_IMM_REG)
        return OP_CLASS_REG_IMM_REG;
    else if (op > OP_CLASS_REG_IMM_REG && op < OP_CLASS_REG_IMM)
        return OP_CLASS_REG_IMM;
    else if (op > OP_CLASS_REG_IMM && op < OP_CLASS_IMM)
        return OP_CLASS_IMM;
    else
        return OP_CLASS_INVALID;
}

const char *opcode_to_str(enum opcode op) {
    assert(op >= 0 && op < OP_CLASS_INVALID);
    return opcode_str[op];
}

enum opcode opcode_from_str(const char *str) {
    int op;
    for (op = 0; op < OP_CLASS_INVALID; op++) {
//        printf("opcode_from_str: %s %s\n", str, opcode_str[op]);
        if (opcode_str[op] && (strcmp(opcode_str[op], str) == 0))
            return op;
    }
    return OP_CLASS_INVALID;
}

int opcode_length(enum opcode op) {
    int bytes = 1;
    if (op > 0 && op < OP_CLASS_NO_OPS)
        return bytes;
    else if (op > OP_CLASS_NO_OPS && op < OP_CLASS_REG)
        return bytes + 4;
    else if (op > OP_CLASS_REG && op < OP_CLASS_REG_REG)
        return bytes + 8;
    else if (op > OP_CLASS_REG_REG && op < OP_CLASS_REG_REG_REG)
        return bytes + 12;
    else if (op > OP_CLASS_REG_REG_REG && op < OP_CLASS_REG_IMM_REG)
        return bytes + 12;
    else if (op > OP_CLASS_REG_IMM_REG && op < OP_CLASS_REG_IMM)
        return bytes + 8;
    else if (op > OP_CLASS_REG_IMM && op < OP_CLASS_IMM)
        return bytes + 4;
    return bytes;
}
